import webpack from 'webpack';
import path from 'path';
import { readdirSync } from 'fs';

export default {
  entry: Object.assign({}, {
    main: './src/index.js'
  }),
  output: {
    filename: 'sailplay-magic.[name].js',
    publicPath: 'dist/',
    sourceMapFilename: './dist/magic.map',
    library: 'magic',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'dist')
  },
  watch: true,
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  resolve: {
    alias: {
      'Components': path.resolve(__dirname, 'src/components/'),
      'Services': path.resolve(__dirname, 'src/services/')
    }
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [['es2015', { modules: false }]],
          plugins: ['syntax-dynamic-import',
            'transform-async-to-generator',
            'transform-regenerator',
            'transform-runtime',
            'transform-custom-element-classes',
            'transform-object-rest-spread']
        }
      }]
    }, {
      test: /\.hbs$/,
      loader: 'handlebars-loader'
    },
    {
      test: /\.css$/,
      use: ['css-loader']
    }]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      warnings: false
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development')
    })
  ]
}