var instance = {},
    actions = {
        new(name) {
            if (instance[name]) throw `Error creating magic: Action ${name} already exists`
            instance[name] = name;
        },

        get() {
            return {...instance}
        }
    }

export default actions    