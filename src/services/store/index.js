import { createStore, combineReducers } from 'redux';
import reducers from './reducers.js';

var instance = null

function createReducer(_reducers) {
    var tmp = reducers.get()
    console.log({tmp, ..._reducers})
    return combineReducers({
        tmp,
        ..._reducers
    });
}

export function configureStore(initialState) {
    if (!instance) {
        instance = createStore(createReducer(), initialState);    
        instance.reducers = {};
    }   

    return instance
}

export function injectReducer(name, reducer) {
    instance.reducers[name] = reducer;
    instance.replaceReducer(createReducer(instance.reducers));
}

export default store => { return instance; }