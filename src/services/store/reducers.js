var instance = {},
    reducers = {
        new(name, fn) {
            if (instance[name]) throw `Error creating magic: Reducer ${name} already exists`
            instance[name] = fn
        },

        get() {
            return {...instance}
        }
    }

export default reducers