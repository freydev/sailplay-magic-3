import store, { configureStore } from 'Services/store';
import actions from 'Services/store/actions'
import reducers from 'Services/store/reducers'
import config from '../../../config/settings.js';
import sp from 'sailplay-hub';

class Magic {
  constructor(element = document.getElementsByTagName('sailplay-magic')[0]) {
    var magic_element = this.magic_element = element;

    if (!magic_element) throw 'Error creating magic: Element not found';

    reducers.new('config', (state = {}, action) => {
      switch (action.type) {
        case 'CONFIG_INIT':
          console.log('config.init')
          return {...action.data}
        default:
          return state  
      }
    })

    configureStore();


    console.log(store().getState())
    for (let widget_conf of config.widgets)
      this.insertWidget(widget_conf).then(widget => widget.register())
  }

  async insertWidget(widget) {
    var cls = await import(`Components/${widget.name}/index.js`);
    this.magic_element.appendChild(document.createElement(cls.widget.elementName));    
    return cls.widget
  }
}

export default Magic