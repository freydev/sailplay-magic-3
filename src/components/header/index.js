import BaseWidget from 'Components/@widget'
import template from './template.hbs'
import css from './style.css'

class SailplayHeader extends BaseWidget {
    constructor() {
        super()
    }

    static get elementName() {
        return 'sailplay-header'
    }

    connectedCallback() {
        BaseWidget.prototype.connectedCallback.call(this)

        this.insertStyle(css);
        this.setTemplate(template, true);
    }
}

export { SailplayHeader as widget } 