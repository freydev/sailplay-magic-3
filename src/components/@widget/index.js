
class BaseWidget extends HTMLElement {
    constructor() {
        super()
    }

    static register() {
        window.customElements.define(this.elementName, this);
    }

    connectedCallback() {
        this.shadow = this.createShadowRoot();
    }

    insertStyle(style) {
        var styleElement = document.createElement('style');
        styleElement.type = 'text/css';
        if (styleElement.styleSheet) { styleElement.styleSheet.cssText = style; }
        else { styleElement.appendChild(document.createTextNode(style)); }

        this.shadow.appendChild(styleElement)
    }

    setTemplate(template, update) {
        var div = document.createElement('div');
        div.innerHTML = template(this.context || {});
        this.templateNodes = div.childNodes;

        if (update) {
            [].slice.call(this.shadow.childNodes).forEach(el => el.tagName != 'STYLE' && this.removeChild(el))
            this.render()
        }
    }

    render() {
        [].slice.call(this.templateNodes).forEach(el => this.shadow.appendChild(el)) // stupid firefox can't iterate NodeList
    }
}

export default BaseWidget