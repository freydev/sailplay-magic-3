import magic from 'Services/magic'
import './polyfill/webcomponents-sd-ce.min.js'

document.addEventListener('DOMContentLoaded', e => {
    function magicReady() {
        window.SAILPLAY_MAGIC = magic

        const event = new Event("magicReady")        
        document.dispatchEvent(event)
    }

    magicReady()
})